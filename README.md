### Hi there 👋

  
- 🔭 I’m currently a research assistant of the Institüt für Medizinische Statistik.
- 🌱 *Ich verbessere mein Deutsch.*
- 👯 My research includes Bayesian methods for pharmaceutical research such as survival and meta-analysis.
- 💬 Ask me about soccer, drinks and Bra**s**il.
- 📫 How to reach me: renato.panaro@med.uni-goettingen.de
- ⚡ Fun fact: My name was chosen after a soccer player who scored a belly goal one month before I was born.
- :chart_with_upwards_trend: About me: [https://rvpanaro.github.io/](https://rvpanaro.github.io/)

<img src="https://raw.githubusercontent.com/rvpanaro/rvpanaro/master/myoctocat.png" width=300 style="float:right"/>
